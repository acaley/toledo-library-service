package service

import (
	"errors"
	"gitlab.com/acaley/toledo-library-service/auth"
)

type SearchService struct {
	itemService   *ItemService
	patronService *PatronService
}

func NewSearchService(itemService *ItemService, patronService *PatronService) *SearchService {
	return &SearchService{
		itemService:   itemService,
		patronService: patronService,
	}
}

func (svc *SearchService) Find(roles []auth.Role, searchTerm string) (interface{}, error) {
	if !auth.IsAuthorized(auth.Read, roles) {
		return nil, errors.New("action not authorized")
	}

	results := []interface{}{}

	if IsBarcode(searchTerm) {
		item, err := svc.itemService.GetByBarcode(roles, searchTerm)
		if err != nil {
			return nil, errors.New("error during barcode lookup during search: " + err.Error())
		}
		if item != nil {
			results = append(results, item)
		}
	}

	//if it's a valid phone num search for it in patrons
	if isPhoneNum(searchTerm) {
		patron, err := svc.patronService.GetByPhoneNum(roles, searchTerm)
		if err != nil {
			return nil, errors.New("error during phonenum lookup during search: " + err.Error())
		}
		if patron != nil {
			results = append(results, patron)
		}
	}

	//skip searching deeper on books or patrons if we found one by barcode or phone num
	if len(results) > 0 {
		return results, nil
	}

	//search for patrons full name
	patrons, err := svc.patronService.SearchByPatronName(roles, searchTerm)
	if err != nil {
		return nil, errors.New("error during patrons lookup during search: " + err.Error())
	}
	if patrons != nil {
		for _, patron := range patrons {
			results = append(results, patron)
		}
	}

	//search for items title and creator
	items, err := svc.itemService.SearchByTitleAndCreator(roles, searchTerm)
	if err != nil {
		return nil, errors.New("error during items lookup during search: " + err.Error())
	}
	if items != nil {
		for _, item := range items {
			results = append(results, item)
		}
	}

	return results, nil
}
