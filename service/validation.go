package service

import (
	"log"
	"regexp"
)

func IsBarcode(barcode string) bool {
	isBarcode, err := regexp.MatchString("(^[0-9]{12}$)|(^[0-9]{10}$)|(^[0-9]{13}$)", barcode)
	if err != nil {
		log.Fatal("Invalid regular expression: " + err.Error())
	}
	return isBarcode
}

func isPhoneNum(phoneNum string) bool {
	isPhoneNum, err := regexp.MatchString("(^[0-9]{10}$)", phoneNum)
	if err != nil {
		log.Fatal("Invalid regular expression: " + err.Error())
	}
	return isPhoneNum
}
