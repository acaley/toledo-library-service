package service

import (
	"errors"
	"gitlab.com/acaley/toledo-library-service/auth"
	"gitlab.com/acaley/toledo-library-service/data"
	"gitlab.com/acaley/toledo-library-service/external"
	"strconv"
)

type Barcode int

const (
	UNKNOWN Barcode = iota
	UPC
	ISBN
	ISBN13
)

type ItemService struct {
	db         external.ItemDb
	itemLookup external.ItemLookup
}

func NewItemService(itemLookup external.ItemLookup, db external.ItemDb) *ItemService {
	return &ItemService{
		db,
		itemLookup,
	}
}

func (svc *ItemService) Create(roles []auth.Role, item *data.Item) (*data.Item, error) {
	if !auth.IsAuthorized(auth.Modify, roles) {
		return nil, errors.New("action not authorized")
	}
	//sanitize item

	//create it in the DB
	return svc.db.Create(item)
}

func (svc *ItemService) Update(roles []auth.Role, id string, item *data.Item) (*data.Item, error) {
	if !auth.IsAuthorized(auth.Modify, roles) {
		return nil, errors.New("action not authorized")
	}
	//sanitize item

	//update in the DB
	return svc.db.Update(id, item)
}

func (svc *ItemService) Delete(roles []auth.Role, id string) (*data.Item, error) {
	if !auth.IsAuthorized(auth.Modify, roles) {
		return nil, errors.New("action not authorized")
	}

	//return deleted
	return svc.db.Delete(id)
}

func (svc *ItemService) Get(roles []auth.Role, id string) (*data.Item, error) {
	if !auth.IsAuthorized(auth.Read, roles) {
		return nil, errors.New("action not authorized")
	}
	return svc.db.Get(id)
}

func (svc *ItemService) GetByBarcode(roles []auth.Role, barcode string) (*data.Item, error) {
	if !auth.IsAuthorized(auth.Read, roles) {
		return nil, errors.New("action not authorized")
	}
	var err error
	t := getBarcodeType(barcode)
	switch t {
	case ISBN:
		barcode, err = convertToIsbn13(barcode)
		if err != nil {
			return nil, errors.New("error converting to isbn 13: " + err.Error()) //wrap err
		}
		fallthrough
	case ISBN13:
		return svc.db.GetByIsbn13(barcode)
	case UPC:
		return svc.db.GetByUpc(barcode)
	default:
		return nil, errors.New("unrecognized barcode type")
	}
}

func (svc *ItemService) CreateFromBarcode(roles []auth.Role, barcode string) (*data.Item, error) {
	if !auth.IsAuthorized(auth.Read, roles) {
		return nil, errors.New("action not authorized")
	}

	item, err := svc.GetByBarcode(roles, barcode)
	if err != nil {
		return nil, errors.New("error looking up item: " + err.Error()) //another case to wrap errors here
	}
	if item != nil {
		return item, nil
	}

	t := getBarcodeType(barcode)
	switch t {
	case ISBN:
		barcode, err = convertToIsbn13(barcode)
		if err != nil {
			return nil, errors.New("error converting to isbn 13: " + err.Error()) //wrap err
		}
		fallthrough
	case ISBN13:
		item, err = svc.itemLookup.LookupItemFromISBN13(barcode)
	case UPC:
		item, err = svc.itemLookup.LookupItemFromUPC(barcode)
	default:
		return nil, errors.New("unrecognized barcode type")
	}

	if err != nil {
		return nil, err
	}

	return svc.db.Create(item)
}

func (svc *ItemService) SearchByTitleAndCreator(roles []auth.Role, searchTerm string) ([]*data.Item, error) {
	if !auth.IsAuthorized(auth.Read, roles) {
		return nil, errors.New("action not authorized")
	}

	return svc.db.SearchByTitleAndCreator(searchTerm)
}

func convertToIsbn13(isbn string) (string, error) {
	isbn13 := "978" + isbn
	checksum := 0
	for index, _ := range isbn13[0:12] {
		value, err := strconv.Atoi(isbn13[index : index+1])
		if err != nil {
			return "", err
		}
		if index%2 == 1 {
			value *= 3
		}
		checksum += value
	}
	checksum = (checksum * 9) % 10
	isbn13 = isbn13[0:12] + strconv.Itoa(checksum)
	return isbn13, nil
}

func getBarcodeType(barcode string) Barcode {
	switch len(barcode) {
	case 13:
		return ISBN13
	case 12:
		return UPC
	case 10:
		return ISBN
	}

	return UNKNOWN
}
