package service

import (
	"errors"
	"gitlab.com/acaley/toledo-library-service/auth"
	"gitlab.com/acaley/toledo-library-service/data"
	"gitlab.com/acaley/toledo-library-service/external"
)

type PatronService struct {
	patronDb external.PatronDb
}

func NewPatronService(patronDb external.PatronDb) *PatronService {
	return &PatronService{
		patronDb,
	}
}

func (svc *PatronService) Get(roles []auth.Role, id string) (*data.Patron, error) {
	if !auth.IsAuthorized(auth.Read, roles) {
		return nil, errors.New("action not authorized")
	}
	return svc.patronDb.Get(id)
}

func (svc *PatronService) Update(roles []auth.Role, id string, patron *data.Patron) (*data.Patron, error) {
	if !auth.IsAuthorized(auth.Modify, roles) {
		return nil, errors.New("action not authorized")
	}
	return svc.patronDb.Update(id, patron)
}

func (svc *PatronService) Create(roles []auth.Role, patron *data.Patron) (*data.Patron, error) {
	if !auth.IsAuthorized(auth.Modify, roles) {
		return nil, errors.New("action not authorized")
	}
	return svc.patronDb.Create(patron)
}

func (svc *PatronService) Delete(roles []auth.Role, id string) (*data.Patron, error) {
	if !auth.IsAuthorized(auth.Modify, roles) {
		return nil, errors.New("action not authorized")
	}
	return svc.patronDb.Delete(id)
}

func (svc *PatronService) GetByPhoneNum(roles []auth.Role, phoneNum string) (*data.Patron, error) {
	if !auth.IsAuthorized(auth.Read, roles) {
		return nil, errors.New("action not authorized")
	}
	return svc.patronDb.GetByPhoneNum(phoneNum)
}

func (svc *PatronService) SearchByPatronName(roles []auth.Role, term string) ([]*data.Patron, error) {
	if !auth.IsAuthorized(auth.Read, roles) {
		return nil, errors.New("action not authorized")
	}

	return svc.patronDb.SearchByPatronName(term)
}
