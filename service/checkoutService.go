package service

import (
	"errors"
	"gitlab.com/acaley/toledo-library-service/auth"
	"gitlab.com/acaley/toledo-library-service/data"
	"gitlab.com/acaley/toledo-library-service/external"
)

type CheckoutService struct {
	checkoutDb external.CheckoutDb
}

func NewCheckoutService(checkoutDb external.CheckoutDb) *CheckoutService {
	return &CheckoutService{
		checkoutDb,
	}
}

func (svc *CheckoutService) Get(roles []auth.Role, id string) (*data.Checkout, error) {
	if !auth.IsAuthorized(auth.Read, roles) {
		return nil, errors.New("action not authorized")
	}

	return svc.checkoutDb.Get(id)
}

func (svc *CheckoutService) Create(roles []auth.Role, checkout *data.Checkout) (*data.Checkout, error) {
	if !auth.IsAuthorized(auth.Modify, roles) {
		return nil, errors.New("action not authorized")
	}

	return svc.checkoutDb.Create(checkout)
}

func (svc *CheckoutService) Update(roles []auth.Role, id string, checkout *data.Checkout) (*data.Checkout, error) {
	if !auth.IsAuthorized(auth.Modify, roles) {
		return nil, errors.New("action not authorized")
	}
	return svc.checkoutDb.Update(id, checkout)
}

func (svc *CheckoutService) Delete(roles []auth.Role, id string) (*data.Checkout, error) {
	if !auth.IsAuthorized(auth.Modify, roles) {
		return nil, errors.New("action not authorized")
	}

	return svc.checkoutDb.Delete(id)
}
