package schema

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/acaley/toledo-library-service/service"
)

func NewLibrarySchema(patronService *service.PatronService, itemService *service.ItemService, checkoutService *service.CheckoutService, searchService *service.SearchService) (graphql.Schema, error) {
	patronSchema := NewPatronSchema(patronService)
	itemSchema := NewItemSchema(itemService)
	checkoutSchema := NewCheckoutSchema(checkoutService, itemService, patronService, patronSchema, itemSchema)
	searchSchema := NewSearchSchema(patronSchema, itemSchema, searchService)

	librarySchema, err := graphql.NewSchema(graphql.SchemaConfig{
		Query: graphql.NewObject(graphql.ObjectConfig{
			Name: "Query",
			Fields: graphql.Fields{
				"patrons": &graphql.Field{
					Type:    patronSchema.object,
					Args:    patronSchema.idField,
					Resolve: patronSchema.resolver,
				},
				"items": &graphql.Field{
					Type:    itemSchema.object,
					Args:    itemSchema.args,
					Resolve: itemSchema.resolver,
				},
				"checkouts": &graphql.Field{
					Type:    checkoutSchema.object,
					Args:    checkoutSchema.args,
					Resolve: checkoutSchema.resolver,
				},
				"search": &graphql.Field{
					Type:    searchSchema.object,
					Args:    searchSchema.args,
					Resolve: searchSchema.resolver,
				},
			},
		}),
		Mutation: graphql.NewObject(graphql.ObjectConfig{
			Name: "Mutation",
			Fields: graphql.Fields{
				"createPatron": &graphql.Field{
					Type:    patronSchema.object,
					Args:    patronSchema.createFields,
					Resolve: patronSchema.create,
				},
				"updatePatron": &graphql.Field{
					Type:    patronSchema.object,
					Args:    patronSchema.updateFields,
					Resolve: patronSchema.update,
				},
				"deletePatron": &graphql.Field{
					Type:    patronSchema.object,
					Args:    patronSchema.idField,
					Resolve: patronSchema.delete,
				},
				"createItem": &graphql.Field{
					Type:    itemSchema.object,
					Args:    itemSchema.createFields,
					Resolve: itemSchema.create,
				},
				"updateItem": &graphql.Field{
					Type:    itemSchema.object,
					Args:    itemSchema.updateFields,
					Resolve: itemSchema.update,
				},
				"deleteItem": &graphql.Field{
					Type:    itemSchema.object,
					Args:    itemSchema.idField,
					Resolve: itemSchema.delete,
				},
				"createCheckout": &graphql.Field{
					Type:    checkoutSchema.object,
					Args:    checkoutSchema.createFields,
					Resolve: checkoutSchema.create,
				},
				"updateCheckout": &graphql.Field{
					Type:    checkoutSchema.object,
					Args:    checkoutSchema.updateFields,
					Resolve: checkoutSchema.update,
				},
				"deleteCheckout": &graphql.Field{
					Type:    checkoutSchema.object,
					Args:    checkoutSchema.idField,
					Resolve: checkoutSchema.delete,
				},
			},
		}),
	})

	return librarySchema, err
}
