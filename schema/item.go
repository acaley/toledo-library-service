package schema

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/acaley/toledo-library-service/auth"
	"gitlab.com/acaley/toledo-library-service/data"
	"gitlab.com/acaley/toledo-library-service/service"
)

type ItemSchema struct {
	object       *graphql.Object
	itemService  *service.ItemService
	args         graphql.FieldConfigArgument
	idField      graphql.FieldConfigArgument
	createFields graphql.FieldConfigArgument
	updateFields graphql.FieldConfigArgument
}

func (schema ItemSchema) resolver(p graphql.ResolveParams) (interface{}, error) {
	id, ok := p.Args["id"].(string)
	if ok {
		return schema.itemService.Get(auth.GetRolesFromParams(p.Source), id)
	}

	barcode, ok := p.Args["barcode"].(string)
	if ok {
		return schema.itemService.GetByBarcode(auth.GetRolesFromParams(p.Source), barcode)
	}

	return nil, nil
}

func (schema ItemSchema) create(p graphql.ResolveParams) (interface{}, error) {
	barcode, ok := p.Args["barcode"].(string)
	if ok {
		return schema.itemService.CreateFromBarcode(auth.GetRolesFromParams(p.Source), barcode)
	}
	return schema.itemService.Create(auth.GetRolesFromParams(p.Source), itemFromArgs(p))

}

func (schema ItemSchema) update(p graphql.ResolveParams) (interface{}, error) {
	id := p.Args["id"].(string)
	return schema.itemService.Update(auth.GetRolesFromParams(p.Source), id, itemFromArgs(p))
}

func (schema ItemSchema) delete(p graphql.ResolveParams) (interface{}, error) {
	id := p.Args["id"].(string)
	return schema.itemService.Delete(auth.GetRolesFromParams(p.Source), id)
}

func itemFromArgs(p graphql.ResolveParams) *data.Item {
	return &data.Item{
		Isbn13:   p.Args["isbn13"].(string),
		Upc:      p.Args["upc"].(string),
		Title:    p.Args["title"].(string),
		Creator:  p.Args["creator"].(string),
		ImageUrl: p.Args["imageUrl"].(string),
		WebUrl:   p.Args["webUrl"].(string),
		ItemType: p.Args["itemType"].(int),
	}
}

func NewItemSchema(itemService *service.ItemService) ItemSchema {

	idArg := &graphql.ArgumentConfig{
		Type:        graphql.NewNonNull(graphql.String),
		Description: "Id of the Patron",
	}
	strArg := &graphql.ArgumentConfig{
		Type: graphql.String,
	}
	intArg := &graphql.ArgumentConfig{
		Type: graphql.Int,
	}

	var itemTypeEnum = graphql.NewEnum(graphql.EnumConfig{
		Name:        "ItemType",
		Description: "Type of library item",
		Values: graphql.EnumValueConfigMap{
			"BOOK": &graphql.EnumValueConfig{
				Value:       0,
				Description: "Book",
			},
			"DVD": &graphql.EnumValueConfig{
				Value:       1,
				Description: "DVD Disc",
			},
			"VHS": &graphql.EnumValueConfig{
				Value:       2,
				Description: "VHS cassette",
			},
		},
	})

	return ItemSchema{
		itemService: itemService,
		object: graphql.NewObject(graphql.ObjectConfig{
			Name:        "Item",
			Description: "An item in the library",
			Fields: graphql.Fields{
				"id": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "The id of the item",
				},
				"isbn13": &graphql.Field{
					Type:        graphql.String,
					Description: "The ISBN code of the item",
				},
				"upc": &graphql.Field{
					Type:        graphql.String,
					Description: "The UPC of the item",
				},
				"title": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "The title of the item",
				},
				"creator": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "The creator of the item",
				},
				"imageUrl": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "The url to an image of the item",
				},
				"webUrl": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "The amazon url to the item",
				},
				"itemType": &graphql.Field{
					Type:        itemTypeEnum,
					Description: "The type of the item",
				},
			},
		}),
		args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type:        graphql.String,
				Description: "Id of the Item",
			},
			"barcode": &graphql.ArgumentConfig{
				Type:        graphql.String,
				Description: "barcode on the item",
			},
		},
		idField: graphql.FieldConfigArgument{
			"id": idArg,
		},
		createFields: graphql.FieldConfigArgument{
			"barcode":  strArg,
			"isbn13":   strArg,
			"upc":      strArg,
			"title":    strArg,
			"creator":  strArg,
			"imageUrl": strArg,
			"webUrl":   strArg,
			"itemType": intArg,
		},
		updateFields: graphql.FieldConfigArgument{
			"id":       idArg,
			"isbn13":   strArg,
			"upc":      strArg,
			"title":    strArg,
			"creator":  strArg,
			"imageUrl": strArg,
			"webUrl":   strArg,
			"itemType": intArg,
		},
	}
}
