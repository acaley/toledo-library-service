package schema

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/acaley/toledo-library-service/auth"
	"gitlab.com/acaley/toledo-library-service/data"
	"gitlab.com/acaley/toledo-library-service/service"
)

type SearchSchema struct {
	searchService *service.SearchService
	object        *graphql.List
	args          graphql.FieldConfigArgument
}

func (schema SearchSchema) resolver(p graphql.ResolveParams) (interface{}, error) {
	searchTerm, ok := p.Args["searchTerm"].(string)
	if !ok {
		return nil, nil
	}
	return schema.searchService.Find(auth.GetRolesFromParams(p.Source), searchTerm)
}

func NewSearchSchema(patronSchema PatronSchema, itemSchema ItemSchema, searchService *service.SearchService) SearchSchema {
	return SearchSchema{
		searchService: searchService,
		object: graphql.NewList(graphql.NewUnion(graphql.UnionConfig{
			Name:        "Search",
			Description: "Search across patrons and items",
			Types: []*graphql.Object{
				patronSchema.object,
				itemSchema.object,
			},
			ResolveType: func(p graphql.ResolveTypeParams) *graphql.Object {
				if _, ok := p.Value.(*data.Patron); ok {
					return patronSchema.object
				}
				return itemSchema.object
			},
		})),
		args: graphql.FieldConfigArgument{
			"searchTerm": &graphql.ArgumentConfig{
				Type:        graphql.NewNonNull(graphql.String),
				Description: "Substring term to search across patrons and items",
			},
		},
	}
}
