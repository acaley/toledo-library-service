package schema

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/acaley/toledo-library-service/auth"
	"gitlab.com/acaley/toledo-library-service/data"
	"gitlab.com/acaley/toledo-library-service/service"
)

type PatronSchema struct {
	patronService *service.PatronService
	object        *graphql.Object
	idField       graphql.FieldConfigArgument
	createFields  graphql.FieldConfigArgument
	updateFields  graphql.FieldConfigArgument
}

func (schema PatronSchema) resolver(p graphql.ResolveParams) (interface{}, error) {
	id, ok := p.Args["id"].(string)
	if !ok {
		return nil, nil
	}
	return schema.patronService.Get(auth.GetRolesFromParams(p.Source), id)
}

func (schema PatronSchema) create(p graphql.ResolveParams) (interface{}, error) {
	return schema.patronService.Create(auth.GetRolesFromParams(p.Source), patronFromArgs(p))
}

func (schema PatronSchema) update(p graphql.ResolveParams) (interface{}, error) {
	id := p.Args["id"].(string)
	return schema.patronService.Update(auth.GetRolesFromParams(p.Source), id, patronFromArgs(p))
}

func (schema PatronSchema) delete(p graphql.ResolveParams) (interface{}, error) {
	id := p.Args["id"].(string)
	return schema.patronService.Delete(auth.GetRolesFromParams(p.Source), id)
}

func patronFromArgs(p graphql.ResolveParams) *data.Patron {
	return &data.Patron{
		FirstName: p.Args["firstName"].(string),
		LastName:  p.Args["lastName"].(string),
		PhoneNum:  p.Args["phoneNum"].(string),
	}
}

func NewPatronSchema(patronService *service.PatronService) PatronSchema {
	idArg := &graphql.ArgumentConfig{
		Type:        graphql.NewNonNull(graphql.String),
		Description: "Id of the Patron",
	}
	strArg := &graphql.ArgumentConfig{
		Type: graphql.NewNonNull(graphql.String),
	}

	return PatronSchema{
		object: graphql.NewObject(graphql.ObjectConfig{
			Name:        "Patron",
			Description: "A patron of the library",
			Fields: graphql.Fields{
				"id": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "The id of the patron",
				},
				"firstName": &graphql.Field{
					Type:        graphql.String,
					Description: "First name of the library patron",
				},
				"lastName": &graphql.Field{
					Type:        graphql.String,
					Description: "Last name of the library patron",
				},
				"phoneNum": &graphql.Field{
					Type:        graphql.String,
					Description: "Phone number of the library patron",
				},
			},
		}),
		idField: graphql.FieldConfigArgument{
			"id": idArg,
		},
		patronService: patronService,
		createFields: graphql.FieldConfigArgument{
			"firstName": strArg,
			"lastName":  strArg,
			"phoneNum":  strArg,
		},
		updateFields: graphql.FieldConfigArgument{
			"id":        idArg,
			"firstName": strArg,
			"lastName":  strArg,
			"phoneNum":  strArg,
		},
	}
}
