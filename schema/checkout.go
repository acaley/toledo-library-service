package schema

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/acaley/toledo-library-service/auth"
	"gitlab.com/acaley/toledo-library-service/data"
	"gitlab.com/acaley/toledo-library-service/service"
)

type CheckoutSchema struct {
	checkoutService *service.CheckoutService
	object          *graphql.Object
	args            graphql.FieldConfigArgument
	idField         graphql.FieldConfigArgument
	createFields    graphql.FieldConfigArgument
	updateFields    graphql.FieldConfigArgument
}

func (schema CheckoutSchema) resolver(p graphql.ResolveParams) (interface{}, error) {
	id, ok := p.Args["id"].(string)
	if ok {
		return schema.checkoutService.Get(auth.GetRolesFromParams(p.Source), id)
	}
	return nil, nil
}

func (schema CheckoutSchema) create(p graphql.ResolveParams) (interface{}, error) {
	return schema.checkoutService.Create(auth.GetRolesFromParams(p.Source), checkoutFromArgs(p))
}

func (schema CheckoutSchema) update(p graphql.ResolveParams) (interface{}, error) {
	id := p.Args["id"].(string)
	return schema.checkoutService.Update(auth.GetRolesFromParams(p.Source), id, checkoutFromArgs(p))
}

func (schema CheckoutSchema) delete(p graphql.ResolveParams) (interface{}, error) {
	id := p.Args["id"].(string)
	return schema.checkoutService.Delete(auth.GetRolesFromParams(p.Source), id)
}

func checkoutFromArgs(p graphql.ResolveParams) *data.Checkout {
	return &data.Checkout{
		PatronId: p.Args["patronId"].(string),
		ItemId:   p.Args["itemId"].(string),
		Time:     p.Args["time"].(string),
		Reminded: p.Args["reminded"].(string),
	}
}

func NewCheckoutSchema(checkoutService *service.CheckoutService, itemService *service.ItemService, patronService *service.PatronService, patron PatronSchema, item ItemSchema) CheckoutSchema {
	idArg := &graphql.ArgumentConfig{
		Type:        graphql.NewNonNull(graphql.String),
		Description: "Id of the Patron",
	}
	strArg := &graphql.ArgumentConfig{
		Type: graphql.NewNonNull(graphql.String),
	}

	return CheckoutSchema{
		object: graphql.NewObject(graphql.ObjectConfig{
			Name:        "Checkout",
			Description: "An checkout from the library",
			Fields: graphql.Fields{
				"id": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "The id of the checkout",
				},
				"patron": &graphql.Field{
					Type:        patron.object,
					Description: "The patron who checked out an item",
					Resolve: func(p graphql.ResolveParams) (i interface{}, err error) {
						s, _ := p.Source.(*data.Checkout)
						return patronService.Get(auth.GetRolesFromParams(p.Info.RootValue), s.PatronId)
					},
				},
				"item": &graphql.Field{
					Type:        item.object,
					Description: "The item that was checked out",
					Resolve: func(p graphql.ResolveParams) (i interface{}, err error) {
						s, _ := p.Source.(*data.Checkout)
						return itemService.Get(auth.GetRolesFromParams(p.Info.RootValue), s.ItemId)
					},
				},
				"time": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.DateTime),
					Description: "The time of the checkout",
					Resolve: func(p graphql.ResolveParams) (i interface{}, err error) {
						s, _ := p.Source.(*data.Checkout)
						return graphql.DateTime.ParseValue(s.Time), nil
					},
				},
				"reminded": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.DateTime),
					Description: "The the last time the patron was reminded of their checkout",
					Resolve: func(p graphql.ResolveParams) (i interface{}, err error) {
						s, _ := p.Source.(*data.Checkout)
						return graphql.DateTime.ParseValue(s.Reminded), nil
					},
				},
			},
		}),
		args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type:        graphql.NewNonNull(graphql.String),
				Description: "Id of the Patron",
			},
		},
		idField: graphql.FieldConfigArgument{
			"id": idArg,
		},
		checkoutService: checkoutService,
		createFields: graphql.FieldConfigArgument{
			"patronId": strArg,
			"itemId":   strArg,
			"time":     strArg,
			"reminded": strArg,
		},
		updateFields: graphql.FieldConfigArgument{
			"id":       idArg,
			"patronId": strArg,
			"itemId":   strArg,
			"time":     strArg,
			"reminded": strArg,
		},
	}
}
