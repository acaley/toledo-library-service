package data

type Item struct {
	Id       string
	Isbn13   string
	Upc      string
	Title    string
	Creator  string
	ImageUrl string
	WebUrl   string
	ItemType int
}
