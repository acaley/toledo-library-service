package data

type Patron struct {
	Id        string
	FirstName string
	LastName  string
	PhoneNum  string
}
