package data

type Checkout struct {
	Id       string
	PatronId string
	ItemId   string
	Time     string
	Reminded string
}
