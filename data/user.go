package data

type User struct {
	Id       string   `json:"id"`
	Password string   `json:"password"`
	Roles    []string `json:"roles"`
}
