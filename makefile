# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=toledo-library-service
BINARY_UNIX=$(BINARY_NAME)_unix

all: test build
build:
		$(GOBUILD) -o $(BINARY_NAME) -v
test:
		$(GOTEST) -v ./...
clean:
		$(GOCLEAN)
		rm -f $(BINARY_NAME)
		rm -f $(BINARY_UNIX)
run:
		$(GOBUILD) -o $(BINARY_NAME) -v ./...
		./$(BINARY_NAME)
deps:
		$(GOGET) github.com/graphql-go/handler
		$(GOGET) github.com/graphql-go/graphql
		$(GOGET) github.com/aws/aws-sdk-go
		$(GOGET) github.com/google/uuid
		$(GOGET) github.com/DDRBoxman/go-amazon-product-api
		$(GOGET) github.com/dgrijalva/jwt-go
		$(GOGET) github.com/aws/aws-sdk-go/service/secretsmanager
		$(GOGET) golang.org/x/crypto/bcrypt
		$(GOGET) golang.org/x/crypto/acme
		$(GOGET) github.com/danilobuerger/autocert-s3-cache
