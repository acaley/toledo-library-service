package external

import "gitlab.com/acaley/toledo-library-service/data"

type ItemLookup interface {
	LookupItemFromUPC(upc string) (*data.Item, error)
	LookupItemFromISBN13(isbn string) (*data.Item, error)
}
