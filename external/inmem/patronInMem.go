package inmem

import (
	"gitlab.com/acaley/toledo-library-service/data"
	"strconv"
)

type PatronInMem struct {
	idMax int
	data  map[string]*data.Patron
}

func NewPatonInMem() *PatronInMem {
	return &PatronInMem{
		idMax: 0,
		data:  map[string]*data.Patron{},
	}
}

func (db PatronInMem) Get(id string) (*data.Patron, error) {
	return db.data[id], nil
}

func (db *PatronInMem) Create(patron *data.Patron) (*data.Patron, error) {
	id := strconv.Itoa(db.idMax)
	db.idMax = db.idMax + 1
	patron.Id = id
	db.data[id] = patron

	return db.Get(id)
}

func (db *PatronInMem) Update(id string, patron *data.Patron) (*data.Patron, error) {
	patron.Id = id
	db.data[id] = patron
	return patron, nil
}

func (db PatronInMem) Delete(id string) (*data.Patron, error) {
	result := db.data[id]
	delete(db.data, id)
	return result, nil
}
