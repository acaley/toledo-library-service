package inmem

import (
	"gitlab.com/acaley/toledo-library-service/data"
	"strconv"
)

type CheckoutInMem struct {
	idMax int
	data  map[string]*data.Checkout
}

func NewCheckoutInMem() *CheckoutInMem {
	return &CheckoutInMem{
		idMax: 0,
		data:  map[string]*data.Checkout{},
	}
}

func (db CheckoutInMem) Get(id string) (*data.Checkout, error) {
	return db.data[id], nil
}

func (db *CheckoutInMem) Create(checkout *data.Checkout) (*data.Checkout, error) {
	id := strconv.Itoa(db.idMax)
	db.idMax = db.idMax + 1
	checkout.Id = id
	db.data[id] = checkout
	return db.Get(id)
}

func (db CheckoutInMem) Update(id string, checkout *data.Checkout) (*data.Checkout, error) {
	checkout.Id = id
	db.data[id] = checkout
	return checkout, nil
}

func (db CheckoutInMem) Delete(id string) (*data.Checkout, error) {
	result := db.data[id]
	delete(db.data, id)
	return result, nil
}
