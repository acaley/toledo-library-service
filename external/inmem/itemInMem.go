package inmem

import (
	"gitlab.com/acaley/toledo-library-service/data"
	"strconv"
)

type ItemInMem struct {
	idMax int
	data  map[string]*data.Item
}

func NewItemInMemoryDb() *ItemInMem {
	return &ItemInMem{
		idMax: 0,
		data:  map[string]*data.Item{},
	}
}

func (db ItemInMem) Get(id string) (*data.Item, error) {
	return db.data[id], nil
}

func (db *ItemInMem) Create(item *data.Item) (*data.Item, error) {
	id := strconv.Itoa(db.idMax)
	db.idMax = db.idMax + 1
	item.Id = id
	db.data[id] = item

	return db.Get(id)
}

func (db ItemInMem) Update(id string, item *data.Item) (*data.Item, error) {
	item.Id = id
	db.data[id] = item
	return item, nil
}

func (db ItemInMem) Delete(id string) (*data.Item, error) {
	result := db.data[id]
	delete(db.data, id)
	return result, nil
}
