package inmem

type InMemAppSecrets struct {
	secret              []byte
	productApiKey       string
	productApiSecretKey string
	productApiTag       string
}

func NewInMemAppSecrets(signingSecret string, productApiKey string, produceApiSecretKey string, productApiTag string) *InMemAppSecrets {
	return &InMemAppSecrets{
		secret:              []byte(signingSecret),
		productApiKey:       productApiKey,
		productApiSecretKey: produceApiSecretKey,
		productApiTag:       productApiTag,
	}
}

func (secretGen *InMemAppSecrets) GetSigningSecret() ([]byte, error) {
	return secretGen.secret, nil
}

func (secretGen *InMemAppSecrets) GetProductApiAccessKey() string {
	return secretGen.productApiKey
}

func (secretGen *InMemAppSecrets) GetProductApiSecretKey() string {
	return secretGen.productApiSecretKey
}

func (secretGen *InMemAppSecrets) GetProductApiAssociateTag() string {
	return secretGen.productApiTag
}
