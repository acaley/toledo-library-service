package inmem

import (
	"gitlab.com/acaley/toledo-library-service/auth"
	"gitlab.com/acaley/toledo-library-service/data"
)

type UserInMem struct {
	data map[string]*data.User
}

func NewUserInMem() *UserInMem {
	return &UserInMem{
		data: map[string]*data.User{
			"admin": {
				Id:       "admin",
				Password: "JDJhJDA0JHRWT00zYnlzOGtuSk84S2E3MzdZaHVKLkNQOXJWaGlmekF4LkVTZDhUb1p0WENQY1BxQUdt", //admin
				Roles: []string{
					string(auth.FullControl),
				},
			},
			"readOnly": {
				Id:       "read",
				Password: "JDJhJDA0JFFoVE5aLnE5Z2V3NWU3cnhaRmh5Ry4zaHhWMUp1QXR3enAuMXlYR0plekNtZW1kM05YMjRH", //read
				Roles: []string{
					string(auth.ReadOnly),
				},
			},
		},
	}
}

func (inmem *UserInMem) Get(id string) (*data.User, error) {
	user := inmem.data[id]
	return user, nil
}
