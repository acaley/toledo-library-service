package inmem

import "gitlab.com/acaley/toledo-library-service/data"

type NoItemLookup struct {
}

func NewNoItemLookup() *NoItemLookup {
	return &NoItemLookup{}
}

func (noItemLookup *NoItemLookup) LookupItemFromUPC(upc string) (*data.Item, error) {
	return nil, nil
}

func (noItemLookup *NoItemLookup) LookupItemFromISBN13(isbn string) (*data.Item, error) {
	return nil, nil
}
