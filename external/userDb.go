package external

import "gitlab.com/acaley/toledo-library-service/data"

type UserDb interface {
	Get(id string) (*data.User, error)
}
