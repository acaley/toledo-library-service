package awsProductApi

import (
	"encoding/xml"
	"errors"
	"github.com/DDRBoxman/go-amazon-product-api"
	"gitlab.com/acaley/toledo-library-service/data"
)

type AwsProductApi struct {
	api amazonproduct.AmazonProductAPI
}

func NewAwsProductApi(accessKey string, secretKey string, associateTag string) (*AwsProductApi, error) {
	api := amazonproduct.AmazonProductAPI{
		AccessKey:    accessKey,
		SecretKey:    secretKey,
		AssociateTag: associateTag,
		Host:         "webservices.amazon.com",
	}

	return &AwsProductApi{
		api,
	}, nil
}
func (aws *AwsProductApi) LookupItemFromUPC(upc string) (*data.Item, error) {
	return aws.lookupItemFromBarcode(upc, "UPC")
}

func (aws *AwsProductApi) LookupItemFromISBN13(isbn string) (*data.Item, error) {
	return aws.lookupItemFromBarcode(isbn, "ISBN")
}

func (aws *AwsProductApi) lookupItemFromBarcode(barcode string, lookupType string) (*data.Item, error) {
	result, err := aws.api.ItemLookupWithParams(map[string]string{
		"Version":         "2009-03-31",
		"ResponseGroup":   "Images,ItemAttributes,Small",
		"SearchIndex":     "All",
		"IdType":          lookupType,
		"ItemId":          barcode,
		"Item.1.Quantity": "1",
	})

	if err != nil {
		return nil, err
	}

	lookupResponse := new(amazonproduct.ItemLookupResponse)
	err = xml.Unmarshal([]byte(result), lookupResponse)
	if err != nil {
		return nil, err
	}

	if len(lookupResponse.Items.Item) == 0 {
		return nil, errors.New("did not find any items in product lookup")
	}

	lookupItem := lookupResponse.Items.Item[0]

	var itemCreator string
	var itemType int
	switch lookupItem.ItemAttributes.ProductGroup {
	case "DVD":
		itemType = 1
		itemCreator = lookupItem.ItemAttributes.Publisher
		break
	case "Book":
		itemType = 0
		itemCreator = lookupItem.ItemAttributes.Author[0]
		break
	case "VHS":
	case "Video":
		itemType = 2
		itemCreator = lookupItem.ItemAttributes.Publisher
		break
	default:
	}

	isbn := ""
	upc := ""

	if lookupType == "ISBN" {
		isbn = barcode
	} else {
		upc = barcode
	}

	return &data.Item{
		Isbn13:   isbn,
		Upc:      upc,
		Title:    lookupItem.ItemAttributes.Title,
		Creator:  itemCreator,
		ImageUrl: lookupItem.LargeImage.URL,
		WebUrl:   lookupItem.DetailPageURL,
		ItemType: itemType,
	}, nil
}
