package dynamo

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"gitlab.com/acaley/toledo-library-service/data"
)

const userTableName = "Users"

type UserDynamoDb struct {
	db *dynamodb.DynamoDB
}

func NewUserDynamoDb(db *dynamodb.DynamoDB) (*UserDynamoDb, error) {
	itemDb := &UserDynamoDb{db}
	err := itemDb.InitDb()
	if err != nil {
		return nil, err
	}
	return itemDb, nil
}

func (udb UserDynamoDb) InitDb() error {
	_, err := udb.db.DescribeTable(&dynamodb.DescribeTableInput{
		TableName: aws.String(userTableName),
	})
	if err == nil {
		return nil
	}

	_, err = udb.db.CreateTable(&dynamodb.CreateTableInput{
		TableName: aws.String(userTableName),
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("id"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("id"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(1),
			WriteCapacityUnits: aws.Int64(1),
		},
	})
	return err
}

func (udb UserDynamoDb) Get(id string) (*data.User, error) {
	doc, err := getDynamoDocFromTable(udb.db, id, userTableName)
	if err != nil {
		return nil, err
	}
	if doc == nil {
		return nil, nil
	}
	return docToUser(doc), nil
}

func docToUser(doc map[string]*dynamodb.AttributeValue) *data.User {
	awsRoles := doc["roles"].SS
	roles := make([]string, len(awsRoles))
	for i, role := range awsRoles {
		roles[i] = *role
	}

	return &data.User{
		Id:       getFromDoc("user", doc),
		Password: getFromDoc("password", doc),
		Roles:    roles,
	}
}
