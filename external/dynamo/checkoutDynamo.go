package dynamo

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/google/uuid"
	"gitlab.com/acaley/toledo-library-service/data"
)

const checkoutTableName = "Checkouts"

type CheckoutDynamoDb struct {
	db *dynamodb.DynamoDB
}

func NewCheckoutDynamoDb(db *dynamodb.DynamoDB) (*CheckoutDynamoDb, error) {
	itemDb := &CheckoutDynamoDb{db}
	err := itemDb.InitDb()
	if err != nil {
		return nil, err
	}
	return itemDb, nil
}

func (cdb CheckoutDynamoDb) InitDb() error {
	_, err := cdb.db.DescribeTable(&dynamodb.DescribeTableInput{
		TableName: aws.String(checkoutTableName),
	})
	if err == nil {
		return nil
	}

	_, err = cdb.db.CreateTable(&dynamodb.CreateTableInput{
		TableName: aws.String(checkoutTableName),
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("id"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("id"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(1),
			WriteCapacityUnits: aws.Int64(1),
		},
	})
	return err
}

func (cdb CheckoutDynamoDb) Get(id string) (*data.Checkout, error) {
	doc, err := getDynamoDocFromTable(cdb.db, id, checkoutTableName)
	if err != nil {
		return nil, err
	}
	if doc == nil {
		return nil, nil
	}
	return docToCheckout(doc), nil
}

func (cdb CheckoutDynamoDb) Create(checkout *data.Checkout) (*data.Checkout, error) {
	checkout.Id = uuid.New().String()
	return cdb.Update(checkout.Id, checkout)
}

func (cdb CheckoutDynamoDb) Update(id string, checkout *data.Checkout) (*data.Checkout, error) {
	doc := checkoutToDoc(checkout)
	_, err := putDynamoDocToTable(cdb.db, id, doc, checkoutTableName)
	if err != nil {
		return nil, err
	}
	return checkout, nil
}

func (cdb CheckoutDynamoDb) Delete(id string) (*data.Checkout, error) {
	checkout, err := cdb.Get(id)
	if err != nil {
		return nil, err
	}
	err = deleteDynamoDocFromTable(cdb.db, id, checkoutTableName)
	if err != nil {
		return nil, err
	}
	return checkout, nil
}

func checkoutToDoc(checkout *data.Checkout) map[string]*dynamodb.AttributeValue {
	doc := map[string]*dynamodb.AttributeValue{}
	addToDoc("id", checkout.Id, &doc)
	addToDoc("patronId", checkout.PatronId, &doc)
	addToDoc("itemId", checkout.ItemId, &doc)
	addToDoc("time", checkout.Time, &doc)
	addToDoc("reminded", checkout.Reminded, &doc)
	return doc
}

func docToCheckout(doc map[string]*dynamodb.AttributeValue) *data.Checkout {
	return &data.Checkout{
		Id:       getFromDoc("id", doc),
		PatronId: getFromDoc("patronId", doc),
		ItemId:   getFromDoc("itemId", doc),
		Time:     getFromDoc("time", doc),
		Reminded: getFromDoc("reminded", doc),
	}
}
