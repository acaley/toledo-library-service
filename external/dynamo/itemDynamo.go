package dynamo

import (
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/google/uuid"
	"gitlab.com/acaley/toledo-library-service/data"
	"log"
	"strconv"
)

const itemTableName = "Items"
const itemUpcIndexName = "ItemsUpc"
const itemIsbnIndexName = "ItemsIsbn"

type ItemDynamoDb struct {
	db *dynamodb.DynamoDB
}

func NewItemDynamoDb(db *dynamodb.DynamoDB) (*ItemDynamoDb, error) {
	itemDb := &ItemDynamoDb{db}
	err := itemDb.InitDb()
	if err != nil {
		return nil, err
	}
	return itemDb, nil
}

func (idb ItemDynamoDb) InitDb() error {
	_, err := idb.db.DescribeTable(&dynamodb.DescribeTableInput{
		TableName: aws.String(itemTableName),
	})
	if err == nil {
		return nil
	}

	_, err = idb.db.CreateTable(&dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("id"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("isbn13"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("upc"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("id"),
				KeyType:       aws.String("HASH"),
			},
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String(itemIsbnIndexName),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("isbn13"),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("KEYS_ONLY"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(1),
					WriteCapacityUnits: aws.Int64(1),
				},
			},
			{
				IndexName: aws.String(itemUpcIndexName),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("upc"),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("KEYS_ONLY"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(1),
					WriteCapacityUnits: aws.Int64(1),
				},
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(1),
			WriteCapacityUnits: aws.Int64(1),
		},
		TableName: aws.String(itemTableName),
	})
	return err
}

func (idb ItemDynamoDb) Get(id string) (*data.Item, error) {
	doc, err := getDynamoDocFromTable(idb.db, id, itemTableName)
	if err != nil {
		return nil, err
	}
	if doc == nil {
		return nil, nil
	}
	return docToItem(doc)
}

func (idb ItemDynamoDb) GetByIsbn13(isbn string) (*data.Item, error) {
	doc, err := idb.db.Query(&dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":isbn": {
				S: aws.String(isbn),
			},
		},
		KeyConditionExpression: aws.String("isbn13 = :isbn"),
		IndexName:              aws.String(itemIsbnIndexName),
		TableName:              aws.String(itemTableName),
	})
	if err != nil {
		return nil, err
	}
	if *doc.Count == 0 {
		return nil, nil
	}

	return idb.Get(*doc.Items[0]["id"].S)
}

func (idb ItemDynamoDb) GetByUpc(upc string) (*data.Item, error) {
	doc, err := idb.db.Query(&dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":upc": {
				S: aws.String(upc),
			},
		},
		KeyConditionExpression: aws.String("upc = :upc"),
		IndexName:              aws.String(itemUpcIndexName),
		TableName:              aws.String(itemTableName),
	})
	if err != nil {
		return nil, err
	}
	if *doc.Count == 0 {
		return nil, nil
	}

	return idb.Get(*doc.Items[0]["id"].S)
}

func (idb ItemDynamoDb) Create(item *data.Item) (*data.Item, error) {
	item.Id = uuid.New().String()
	return idb.Update(item.Id, item)
}

func (idb ItemDynamoDb) Update(id string, item *data.Item) (*data.Item, error) {
	doc := itemToDoc(item)
	_, err := putDynamoDocToTable(idb.db, id, doc, itemTableName)
	if err != nil {
		return nil, err
	}
	return item, nil
}

func (idb ItemDynamoDb) Delete(id string) (*data.Item, error) {
	item, err := idb.Get(id)

	if err != nil {
		return nil, err
	}

	err = deleteDynamoDocFromTable(idb.db, id, itemTableName)

	if err != nil {
		return nil, err
	}

	return item, nil
}

func (idb ItemDynamoDb) SearchByTitleAndCreator(term string) ([]*data.Item, error) {
	doc, err := idb.db.Scan(&dynamodb.ScanInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":term": {
				S: aws.String(term),
			},
		},
		FilterExpression: aws.String("(contains(creator, :term) or contains(title, :term))"),
		TableName:        aws.String(itemTableName),
	})
	if err != nil {
		return nil, err
	}
	if *doc.Count == 0 {
		return nil, nil
	}

	items := []*data.Item{}
	for _, doc := range doc.Items {
		item, err := docToItem(doc)
		if err != nil {
			log.Print(errors.New("error creating item during search: " + err.Error()))
			continue
		}
		items = append(items, item)
	}

	return items, nil
}

func docToItem(doc map[string]*dynamodb.AttributeValue) (*data.Item, error) {
	t, err := strconv.Atoi(*doc["itemType"].S)
	if err != nil {
		return nil, fmt.Errorf("item type could not be converted to int")
	}
	return &data.Item{
		Id:       getFromDoc("id", doc),
		Isbn13:   getFromDoc("isbn13", doc),
		Upc:      getFromDoc("upc", doc),
		Title:    getFromDoc("title", doc),
		Creator:  getFromDoc("creator", doc),
		ImageUrl: getFromDoc("imageUrl", doc),
		WebUrl:   getFromDoc("webUrl", doc),
		ItemType: t,
	}, nil
}

func itemToDoc(item *data.Item) map[string]*dynamodb.AttributeValue {
	doc := map[string]*dynamodb.AttributeValue{}
	addToDoc("id", item.Id, &doc)
	addToDoc("isbn13", item.Isbn13, &doc)
	addToDoc("upc", item.Upc, &doc)
	addToDoc("title", item.Title, &doc)
	addToDoc("creator", item.Creator, &doc)
	addToDoc("imageUrl", item.ImageUrl, &doc)
	addToDoc("webUrl", item.WebUrl, &doc)
	addToDoc("itemType", strconv.Itoa(item.ItemType), &doc)
	return doc
}
