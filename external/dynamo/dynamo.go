package dynamo

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func NewDynamoDb(config *aws.Config) (*dynamodb.DynamoDB, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}

	return dynamodb.New(sess, config), nil
}

func deleteDynamoDocFromTable(dynamo *dynamodb.DynamoDB, id string, tableName string) error {
	_, err := dynamo.DeleteItem(&dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	})

	return err
}

func getDynamoDocFromTable(dynamo *dynamodb.DynamoDB, id string, tableName string) (map[string]*dynamodb.AttributeValue, error) {
	result, err := dynamo.GetItem(&dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	})

	if err != nil {
		return nil, err
	}

	return result.Item, nil
}

func putDynamoDocToTable(dynamo *dynamodb.DynamoDB, id string, doc map[string]*dynamodb.AttributeValue, tableName string) (map[string]*dynamodb.AttributeValue, error) {
	doc["id"] = &dynamodb.AttributeValue{
		S: aws.String(id),
	}

	output, err := dynamo.PutItem(&dynamodb.PutItemInput{
		Item:      doc,
		TableName: aws.String(tableName),
	})

	if err != nil {
		return nil, err
	}

	return output.Attributes, nil
}

func getFromDoc(field string, doc map[string]*dynamodb.AttributeValue) string {
	fieldValue := doc[field]
	if fieldValue != nil {
		return *fieldValue.S
	}
	return ""
}

func addToDoc(field string, value string, doc *map[string]*dynamodb.AttributeValue) {
	if value == "" {
		return
	}

	(*doc)[field] = &dynamodb.AttributeValue{
		S: aws.String(value),
	}
}
