package dynamo

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/google/uuid"
	"gitlab.com/acaley/toledo-library-service/data"
)

const patronTableName = "Patrons"
const patronPhoneNumIndexName = "PatronsPhoneNum"
const patronFirstNameIndex = "PatronFirstNameIndex"
const patronLastNameIndex = "PatronLastNameIndex"

type PatronDynamoDb struct {
	db *dynamodb.DynamoDB
}

func NewPatronDynamoDb(db *dynamodb.DynamoDB) (*PatronDynamoDb, error) {
	itemDb := &PatronDynamoDb{db}
	err := itemDb.InitDb()
	if err != nil {
		return nil, err
	}
	return itemDb, nil
}

func (cdb PatronDynamoDb) InitDb() error {
	_, err := cdb.db.DescribeTable(&dynamodb.DescribeTableInput{
		TableName: aws.String(patronTableName),
	})
	if err == nil {
		return nil
	}

	_, err = cdb.db.CreateTable(&dynamodb.CreateTableInput{
		TableName: aws.String(patronTableName),
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("id"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("phoneNum"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("id"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(1),
			WriteCapacityUnits: aws.Int64(1),
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String(patronPhoneNumIndexName),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("phoneNum"),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("KEYS_ONLY"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(1),
					WriteCapacityUnits: aws.Int64(1),
				},
			},
		},
	})
	return err
}

func (pdb PatronDynamoDb) Get(id string) (*data.Patron, error) {
	doc, err := getDynamoDocFromTable(pdb.db, id, patronTableName)
	if err != nil {
		return nil, err
	}
	if doc == nil {
		return nil, nil
	}
	return docToPatron(doc), nil
}

func (pdb PatronDynamoDb) Create(patron *data.Patron) (*data.Patron, error) {
	patron.Id = uuid.New().String()
	return pdb.Update(patron.Id, patron)
}

func (pdb PatronDynamoDb) Update(id string, patron *data.Patron) (*data.Patron, error) {
	doc := patronToDoc(patron)
	_, err := putDynamoDocToTable(pdb.db, id, doc, patronTableName)
	if err != nil {
		return nil, err
	}

	return patron, nil
}

func (pdb PatronDynamoDb) Delete(id string) (*data.Patron, error) {
	item, err := pdb.Get(id)

	if err != nil {
		return nil, err
	}

	err = deleteDynamoDocFromTable(pdb.db, id, itemTableName)

	if err != nil {
		return nil, err
	}

	return item, nil
}

func (cdb PatronDynamoDb) GetByPhoneNum(phoneNum string) (*data.Patron, error) {
	doc, err := cdb.db.Query(&dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":phoneNum": {
				S: aws.String(phoneNum),
			},
		},
		KeyConditionExpression: aws.String("phoneNum = :phoneNum"),
		IndexName:              aws.String(patronPhoneNumIndexName),
		TableName:              aws.String(patronTableName),
	})
	if err != nil {
		return nil, err
	}
	if *doc.Count == 0 {
		return nil, nil
	}
	return cdb.Get(*doc.Items[0]["id"].S)
}

func (cdb PatronDynamoDb) SearchByPatronName(nameSubstring string) ([]*data.Patron, error) {
	doc, err := cdb.db.Scan(&dynamodb.ScanInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":nameSubstring": {
				S: aws.String(nameSubstring),
			},
		},
		FilterExpression: aws.String("begins_with(firstName, :nameSubstring) or begins_with(lastName, :nameSubstring)"),
		TableName:        aws.String(patronTableName),
	})
	if err != nil {
		return nil, err
	}
	if *doc.Count == 0 {
		return nil, nil
	}

	patrons := []*data.Patron{}
	for _, item := range doc.Items {
		patrons = append(patrons, docToPatron(item))
	}

	return patrons, nil
}

func patronToDoc(patron *data.Patron) map[string]*dynamodb.AttributeValue {
	doc := map[string]*dynamodb.AttributeValue{}
	addToDoc("id", patron.Id, &doc)
	addToDoc("firstName", patron.FirstName, &doc)
	addToDoc("lastName", patron.LastName, &doc)
	addToDoc("phoneNum", patron.PhoneNum, &doc)
	return doc
}

func docToPatron(doc map[string]*dynamodb.AttributeValue) *data.Patron {
	return &data.Patron{
		Id:        getFromDoc("id", doc),
		FirstName: getFromDoc("firstName", doc),
		LastName:  getFromDoc("lastName", doc),
		PhoneNum:  getFromDoc("phoneNum", doc),
	}
}
