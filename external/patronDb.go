package external

import "gitlab.com/acaley/toledo-library-service/data"

type PatronDb interface {
	Get(id string) (*data.Patron, error)
	Create(patron *data.Patron) (*data.Patron, error)
	Update(id string, patron *data.Patron) (*data.Patron, error)
	Delete(id string) (*data.Patron, error)
	GetByPhoneNum(phoneNum string) (*data.Patron, error)
	SearchByPatronName(nameSubstring string) ([]*data.Patron, error)
}
