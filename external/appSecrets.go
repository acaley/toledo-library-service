package external

type AppSecrets interface {
	GetSigningSecret() ([]byte, error)
	GetProductApiAccessKey() string
	GetProductApiSecretKey() string
	GetProductApiAssociateTag() string
}
