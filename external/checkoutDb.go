package external

import "gitlab.com/acaley/toledo-library-service/data"

type CheckoutDb interface {
	Get(id string) (*data.Checkout, error)
	Create(checkout *data.Checkout) (*data.Checkout, error)
	Update(id string, checkout *data.Checkout) (*data.Checkout, error)
	Delete(id string) (*data.Checkout, error)
}
