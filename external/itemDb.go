package external

import "gitlab.com/acaley/toledo-library-service/data"

type ItemDb interface {
	Get(id string) (*data.Item, error)
	GetByIsbn13(isbn string) (*data.Item, error)
	GetByUpc(upc string) (*data.Item, error)

	Create(item *data.Item) (*data.Item, error)
	Update(id string, item *data.Item) (*data.Item, error)
	Delete(id string) (*data.Item, error)
	SearchByTitleAndCreator(term string) ([]*data.Item, error)
}
