package awssecretsmanager

// Use this code snippet in your app.
// If you need more information about configurations or implementing the sample code, visit the AWS docs:
// https://docs.aws.amazon.com/sdk-for-go/v1/developer-guide/setting-up.html

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

type AwsSecretManager struct {
	signingSecret          []byte
	productApiAccessKey    string
	productApiSecretKey    string
	productApiAssociateTag string
}

func NewAwsSecretManager(secretsMgr *secretsmanager.SecretsManager) (*AwsSecretManager, error) {
	signingSecretName := "prod/toledo_library/signingSecret"
	productApiAccessKeyName := "prod/toledo_library/product_api/access"
	productApiSecretName := "prod/toledo_library/product_api/secret"
	productApiTag := "prod/toledo_library/product_api/tag"

	result, err := secretsMgr.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: aws.String(signingSecretName),
	})
	if err != nil {
		return nil, err
	}
	signingSecret := []byte(*result.SecretString)

	result, err = secretsMgr.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: aws.String(productApiAccessKeyName),
	})
	if err != nil {
		return nil, err
	}
	productApiAccessKey := *result.SecretString

	result, err = secretsMgr.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: aws.String(productApiSecretName),
	})
	if err != nil {
		return nil, err
	}
	productApiSecretKey := *result.SecretString

	result, err = secretsMgr.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: aws.String(productApiTag),
	})
	if err != nil {
		return nil, err
	}
	productApiAssociateTag := *result.SecretString

	return &AwsSecretManager{
		signingSecret:          signingSecret,
		productApiAccessKey:    productApiAccessKey,
		productApiSecretKey:    productApiSecretKey,
		productApiAssociateTag: productApiAssociateTag,
	}, nil
}

func (secretGen *AwsSecretManager) GetSigningSecret() ([]byte, error) {
	return secretGen.signingSecret, nil
}

func (secretGen *AwsSecretManager) GetProductApiAccessKey() string {
	return secretGen.productApiAccessKey
}

func (secretGen *AwsSecretManager) GetProductApiSecretKey() string {
	return secretGen.productApiSecretKey
}

func (secretGen *AwsSecretManager) GetProductApiAssociateTag() string {
	return secretGen.productApiAssociateTag
}
