package auth

type Role string
type Action string

const (
	FullControl Role = "FullControl"
	ReadOnly    Role = "ReadOnly"
)

const (
	Modify Action = "Modify"
	Read   Action = "Read"
)

func IsAuthorized(action Action, roles []Role) bool {
	for _, role := range roles {
		if role == FullControl {
			return true
		}

		if action == Read && role == ReadOnly {
			return true
		}
	}

	return false
}

func parseRoles(input []string) []Role {
	var roles []Role
	for _, role := range input {
		switch Role(role) {
		case FullControl, ReadOnly:
			roles = append(roles, Role(role))
		default:
			continue
		}
	}
	return roles
}
