package auth

import (
	"log"
)

func GetRolesFromParams(p interface{}) []Role {
	roles, ok := p.(map[string]interface{})["roles"].([]Role)
	if !ok {
		return []Role{}
	}
	return roles
}

func CreateRolesMapFromToken(secret []byte, token string) map[string]interface{} {
	roles, err := getRolesFromToken(secret, token)
	if err != nil {
		log.Print("failed to get roles from token: ", err)
		return map[string]interface{}{"roles": []Role{}}
	}

	return map[string]interface{}{"roles": roles}
}
