package auth

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"time"
)

var timeFormat = time.RFC3339

func createTokenWithRoles(secret []byte, roles []Role, ttl string) (string, string, error) {
	duration, err := time.ParseDuration(ttl)
	if err != nil {
		return "", "", errors.New("invalid token TTL format") //Need to wrap err here
	}

	expiration := time.Now().Add(duration).Format(timeFormat)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"roles":      roles,
		"expiration": expiration,
	})

	tokenString, err := token.SignedString(secret)
	if err != nil {
		return "", "", err //Need to wrap err here
	}

	return tokenString, expiration, nil
}

func getRolesFromToken(secret []byte, t string) ([]Role, error) {
	if t == "" {
		return nil, errors.New("authorization token must be present")
	}
	token, err := jwt.Parse(t, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unable to parse token")
		}
		return secret, nil
	})
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return nil, errors.New("invalid authorization token")
	}

	expires, err := time.Parse(timeFormat, claims["expiration"].(string))

	if err != nil {
		return nil, errors.New("token expiration format is invalid")
	}

	if time.Now().After(expires) {
		return nil, errors.New("token has expired")
	}

	if claims["roles"] == nil {
		return nil, nil
	}

	rolesInterface := claims["roles"].([]interface{})
	roles := make([]string, len(rolesInterface))
	for i, v := range rolesInterface {
		roles[i] = fmt.Sprint(v)
	}
	return parseRoles(roles), nil
}
