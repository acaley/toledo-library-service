package auth

import (
	"encoding/base64"
	"encoding/json"
	"gitlab.com/acaley/toledo-library-service/data"
	"gitlab.com/acaley/toledo-library-service/external"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
)

const (
	TokenTtl = "24h"
	Salt     = "ToledoLibrary"
)

type SecretRetriever interface {
	GetSigningSecret() ([]byte, error)
}

type TokenCreatorEndpoint struct {
	secretRetriever SecretRetriever
	userDb          external.UserDb
}

type TokenResponse struct {
	Token   string `json:"token"`
	Expires string `json:"expires"`
}

type ErrorResponse struct {
	Error string `json:"error"`
}

func NewTokenCreatorEndpoint(secretRetriever SecretRetriever, userDb external.UserDb) *TokenCreatorEndpoint {
	return &TokenCreatorEndpoint{
		secretRetriever,
		userDb,
	}
}

func (creator *TokenCreatorEndpoint) CreateTokenEndpoint(response http.ResponseWriter, request *http.Request) {

	var requestUser data.User
	err := json.NewDecoder(request.Body).Decode(&requestUser)
	if err != nil {
		writeErrorResponse(response, 400, "invalid requestUser object")
		return
	}

	dbUser, err := creator.userDb.Get(requestUser.Id)
	if err != nil {
		log.Print("user not found during lookup: ", err)
		writeErrorResponse(response, 500, "error finding user")
		return
	}

	if dbUser == nil {
		writeErrorResponse(response, 400, "error finding user")
		return
	}
	expectedHash, _ := base64.StdEncoding.DecodeString(dbUser.Password)

	err = bcrypt.CompareHashAndPassword(expectedHash, []byte(requestUser.Password))

	if err != nil {
		log.Print("incorrect password for user: ", dbUser.Id)
		writeErrorResponse(response, 400, "error finding user")
		return
	}

	secret, err := creator.secretRetriever.GetSigningSecret()
	if err != nil {
		log.Print("failed to retrieve signing secret: ", err)
		writeErrorResponse(response, 500, "error creating token")
		return
	}

	roles := parseRoles(dbUser.Roles)
	tokenString, expiration, err := createTokenWithRoles(secret, roles, TokenTtl)
	if err != nil {
		log.Print("failed to create token: ", err)
		writeErrorResponse(response, 500, "error creating token")
		return
	}

	responseBytes, _ := json.Marshal(&TokenResponse{
		Token:   tokenString,
		Expires: expiration,
	})
	response.Header().Set("content-type", "application/json")
	response.Write(responseBytes)
}

func writeErrorResponse(response http.ResponseWriter, code int, message string) {
	errorResponse, _ := json.Marshal(&ErrorResponse{
		Error: message,
	})
	response.WriteHeader(code)
	response.Write(errorResponse)
}
