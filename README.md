# Toledo Library Service

This is the backend service for the toledo library.  

#### Things that you will eventually be able to do
- Add new items by ISBN or UPC
- Search for patrons and items in the library
 
#### Getting Started Developing the service

Go get some dependencies
```bash
make deps
```
Then build and run
```bash
make build
export DYNAMO_ENDPOINT=http://localhost:8000
export IN_MEM_USERS=true
export IN_MEM_SECRET=true
./toledo-library-service
```
By default the service is configured to talk with a local instance of dynamodb, which can be run via docker
```bash
docker run -it -p 8000:8000 amazon/dynamodb-local
```
Now you can talk to the service, first you will want to login to get a token
```bash
curl -X GET localhost:8080/login -d '{"id":"admin","password":"admin"}'
```
Then you can start to explore the graphql API
```bash
curl -X POST -H "TOKEN:<token>" localhost:8080/graphql -d 'mutation {createPatron(
                                          firstName: "John",
                                          lastName: "Doe",
                                          phoneNum: "2065551212",
                                        ){
                                          id
                                          firstName
                                          lastName
                                          phoneNum
                                        }}'
```
And if you want to get fancy, open insomnia, import the provided workspace and explore the schema

#### Configuration Options via Environment Variables

```bash
IN_MEM_USERS <default:false>
IN_MEM_SECRET <default:false>
DYNAMO_ENDPOINT <default:>
PORT <default:8080>
REGION <default:us-west-2>
AWS_ACCESS_KEY <default:>
AWS_SECRET_KEY <default:>
PRODUCT_API_ACCESS_KEY <default:>
PRODUCT_API_SECRET_KEY <default:>
PRODUCT_API_ASSOCIATE_TAG <default:>
```

#### Running service in docker container with linked dynamodb
```bash
docker run -i --rm -p 8080:8080 -e DYNAMO_ENDPOINT=http://dynamodb:8000 --link dynamodb registry.gitlab.com/acaley/toledo-library-service
```