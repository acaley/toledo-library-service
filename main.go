package main

import (
	"context"
	"crypto/tls"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/danilobuerger/autocert-s3-cache"
	"github.com/graphql-go/handler"
	"golang.org/x/crypto/acme/autocert"

	"gitlab.com/acaley/toledo-library-service/auth"
	"gitlab.com/acaley/toledo-library-service/external"
	"gitlab.com/acaley/toledo-library-service/external/awsProductApi"
	"gitlab.com/acaley/toledo-library-service/external/awssecretsmanager"
	"gitlab.com/acaley/toledo-library-service/external/dynamo"
	"gitlab.com/acaley/toledo-library-service/external/inmem"
	"gitlab.com/acaley/toledo-library-service/schema"
	"gitlab.com/acaley/toledo-library-service/service"
)

func main() {
	port := getEnv("PORT", "8080")
	region := getEnv("REGION", "us-west-2")
	productApiAccess := getEnv("PRODUCT_API_ACCESS_KEY", "")
	productApiSecret := getEnv("PRODUCT_API_SECRET_KEY", "")
	productApiTag := getEnv("PRODUCT_API_ASSOCIATE_TAG", "")
	inMemUsers := getEnvBool("IN_MEM_USERS", false)
	inMemSecret := getEnv("IN_MEM_SECRET", "")
	insecure := getEnvBool("INSECURE", false)
	dynamoEndpoint, isEndpointSpecified := os.LookupEnv("DYNAMO_ENDPOINT")

	log.Println("begin app initialization")
	var awsConfig *aws.Config
	if isEndpointSpecified {
		awsConfig = aws.NewConfig().WithRegion(region).WithEndpoint(dynamoEndpoint)
	} else {
		awsConfig = aws.NewConfig().WithRegion(region)
	}
	awsSession, err := session.NewSession()
	if err != nil {
		log.Fatal("error creating AWS session" + err.Error())
	}

	dynamoDb := dynamodb.New(awsSession, awsConfig)
	if err != nil {
		log.Fatal("failure connecting to dynamoDb", err)
		return
	}

	itemDb, err := dynamo.NewItemDynamoDb(dynamoDb)
	if err != nil {
		log.Fatal("error initializing itemDb", err)
		return
	}
	log.Println("itemDb initialized")

	checkoutDb, err := dynamo.NewCheckoutDynamoDb(dynamoDb)
	if err != nil {
		log.Fatal("error initializing checkoutDb", err)
		return
	}
	log.Println("checkoutDb initialized")

	patronDb, err := dynamo.NewPatronDynamoDb(dynamoDb)
	if err != nil {
		log.Fatal("error initializing patronDb", err)
		return
	}
	log.Println("patronDb initialized")

	var userDb external.UserDb
	if inMemUsers {
		userDb = inmem.NewUserInMem()
	} else {
		userDb, err = dynamo.NewUserDynamoDb(dynamoDb)
		if err != nil {
			log.Fatal("error initializing userDb")
			return
		}
	}
	log.Println("userDb initialized")

	var secrets external.AppSecrets
	if inMemSecret != "" {
		secrets = inmem.NewInMemAppSecrets(inMemSecret, productApiAccess, productApiSecret, productApiTag)
	} else {
		secrets, err = awssecretsmanager.NewAwsSecretManager(secretsmanager.New(awsSession, awsConfig))
		if err != nil {
			log.Fatal("failure building secret generator", err)
			return
		}
	}
	log.Println("secret generator initialized")

	itemLookup, err := awsProductApi.NewAwsProductApi(secrets.GetProductApiAccessKey(), secrets.GetProductApiSecretKey(), secrets.GetProductApiAssociateTag())
	if err != nil {
		log.Fatal("error initializing product api")
		return
	}
	log.Println("product api initialized")

	patronService := service.NewPatronService(patronDb)
	itemService := service.NewItemService(itemLookup, itemDb)
	checkoutService := service.NewCheckoutService(checkoutDb)
	searchService := service.NewSearchService(itemService, patronService)

	libSchema, err := schema.NewLibrarySchema(patronService, itemService, checkoutService, searchService)
	if err != nil {
		log.Fatal("failure building library schema", err)
		return
	}
	log.Println("graphql schema initialized")

	tokenCreator := auth.NewTokenCreatorEndpoint(secrets, userDb)

	h := handler.New(&handler.Config{
		Schema: &libSchema,
		RootObjectFn: func(ctx context.Context, r *http.Request) map[string]interface{} {
			token := r.Header.Get("Authorization")
			if token == "" {
				return map[string]interface{}{}
			}
			if token[:len("Bearer ")] != "Bearer " {
				return map[string]interface{}{}
			}
			secret, err := secrets.GetSigningSecret()
			if err != nil {
				log.Fatal("failure to retrieve signing secret")
			}
			return auth.CreateRolesMapFromToken(secret, token[len("Bearer "):])
		},
	})

	http.HandleFunc("/login", tokenCreator.CreateTokenEndpoint)
	http.Handle("/graphql", h)
	log.Println("All initialization completed, service starting on port:" + port)
	if insecure {
		log.Fatal(http.ListenAndServe(":"+port, nil))
	} else {
		cache, err := s3cache.New("us-west-2", "autocert-bucket")
		if err != nil {
			log.Fatal(err)
		}

		m := autocert.Manager{
			Prompt:          autocert.AcceptTOS,
			Cache:           cache,
			HostPolicy:      autocert.HostWhitelist("toledo-library-service.toledolibrary.acaley.com"),
		}

		s := &http.Server{
			Addr:      ":https",
			TLSConfig: &tls.Config{GetCertificate: m.GetCertificate},
		}

		go http.ListenAndServe(":http", m.HTTPHandler(nil))
		log.Printf("start listening at :http")

		log.Fatal(s.ListenAndServeTLS("", "")) // Key and cert provided by Let's Encrypt
	}
}

func getEnv(name string, def string) string {
	value, ok := os.LookupEnv(name)
	if !ok {
		return def
	}
	return value
}

func getEnvBool(name string, def bool) bool {
	value, ok := os.LookupEnv(name)
	if !ok {
		return def
	}
	boolVal, err := strconv.ParseBool(value)
	if err != nil {
		log.Fatal("unable to parse boolean ENV variable: " + name + ": " + err.Error())
	}
	return boolVal
}
